import React, { Component, Fragment } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import "./App.css";
import Header from "./components/header/header";
import routes from "./route";

class App extends Component {
  render() {
    return (
      <Router>
        <Header />
        {routes}
      </Router>
    );
  }
}

export default App;
