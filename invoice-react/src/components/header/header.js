import React, { Component } from "react";
import { Link } from "react-router-dom";

import { Navbar, NavbarBrand, Nav, NavItem, Container } from "reactstrap";

import "./header.css";

class Header extends Component {
  render() {
    return (
      <div>
        <Container>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Invoice App</NavbarBrand>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/">Invoice</Link>
              </NavItem>
              <NavItem>
                <Link to="/products" className="ml-5">
                  Products
                </Link>
              </NavItem>
              <NavItem>
                <Link to="/customers" className="ml-5">
                  Customers
                </Link>
              </NavItem>
            </Nav>
          </Navbar>
        </Container>
      </div>
    );
  }
}

export default Header;
