import React, { Component } from "react";

import {
  Container,
  Row,
  Button,
  Form,
  FormGroup,
  Col,
  Label,
  Input,
  CustomInput,
  Toast,
  ToastBody,
  ToastHeader,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import moment from "moment";

import {
  fetchInvoices,
  postInvoice,
  getCustomers,
  getProducts,
  postAProduct,
  editInvoice,
  deleteInvoice,
  getAProduct
} from "../../services/api";
import "./invoice.css";

class Invoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invoice: [],
      isLoading: true,
      error: null,
      customers: [],
      customersError: null,
      isCustomersLoading: true,
      allProducts: false,
      products: [],
      isProductsLoading: true,
      productsError: null,
      productPrice: [],
      price: [],
      productItems: [],
      selectedCustomer: "",
      productList: [
        {
          aProduct: "",
          quantity: "1"
        }
      ],
      discount: "",
      customerInputError: null,
      productNameError: null,
      quantityInputError: null,
      discountInputError: null,
      total: 0,
      invoicePostedResponse: null,
      disableProdOption: false,
      modal: false,
      editModal: false,
      cusIdOfEditableInvoice: null,
      invoiceId: null,
      aProductPost: null,
      aProductPostError: null,
      editDiscount: "",
      editInvoiceError: null,
      deleteError: null
    };
  }

  componentDidMount() {
    this.fetchInvoices();
    this.getCustomers();
    this.getProducts();
  }

  fetchInvoices() {
    fetchInvoices()
      .then(data =>
        this.setState({
          invoice: data,
          isLoading: false
        })
      )
      .catch(error => this.setState({ isLoading: true, error }));
  }

  toggleInvoiceForm = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  };

  createInvoiceForm = () => {
    this.blankFormField();
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  };

  toggleInvoiceEditForm = () => {
    this.setState(prevState => ({
      editModal: !prevState.editModal
    }));
  };

  openInvoiceEditForm = async (cusId, invoiceId, discount, total) => {
    this.setState(prevState => ({
      editModal: !prevState.editModal,
      invoiceId,
      discount,
      total
    }));
    await this.setValue("selectedCustomer", cusId);
    // await this.setValue("discount", discount);
    console.log("selectedCustomer", this.state.selectedCustomer);
    this.getAProduct(invoiceId);
    console.log("productList", this.state.productList);
    console.log("discount", this.state.discount);
    console.log("total", this.state.total);
    this.clearValidation();
    this.singleProductPrice();
  };

  clearValidation() {
    this.setState({
      customerInputError: null,
      productNameError: null,
      quantityInputError: null,
      discountInputError: null
    });
  }

  getAProduct = id => {
    let productList = [];
    getAProduct(id)
      .then(async productItems => {
        for (let i = 0; i < productItems.length; i++) {
          productList.push({
            aProduct: productItems[i].product_id,
            quantity: productItems[i].quantity
          });
        }
        await this.setValue("productList", productList);
        this.setState({ productItems });
      })
      .catch(err => {
        console.log(err.error);
      });
  };

  async singleProductPrice() {
    const { productList } = this.state;
    if (productList.length > 0) {
      const price = this.state.products
        .filter(prod => productList.find(aProd => prod.id === +aProd.aProduct))
        .map(prod => prod.price);
      // this.setState({ price });
      await this.setValue("price", price);
    }
  }

  getCustomers() {
    getCustomers()
      .then(customers => {
        this.setState({ customers, isCustomersLoading: false });
      })
      .catch(err => {
        this.setState({
          customersError: err.error,
          isCustomersLoading: true
        });
      });
  }

  getProducts() {
    getProducts()
      .then(products => {
        this.setState({ products, isProductsLoading: false });
      })
      .catch(err => {
        this.setState({ productsError: err.error, isProductsLoading: true });
      });
  }

  setValue(name, value) {
    return new Promise((res, rej) => {
      this.setState(
        () => {
          return { [name]: value };
        },
        () => res(this.state)
      );
    });
  }

  handleCustomer = e => {
    this.setState({ selectedCustomer: e.target.value }, () => {
      this.validateCustomerInput();
    });
  };

  validateCustomerInput = () => {
    const { selectedCustomer } = this.state;
    this.setState({
      customerInputError:
        selectedCustomer !== "" ? null : "Please select customer"
    });
  };

  handleDiscount = e => {
    this.setState({ discount: e.target.value }, () => {
      this.validateDiscount();
    });
    // this.editedTotal();
  };

  validateDiscount = () => {
    const { discount } = this.state;
    this.setState({
      discountInputError: discount !== "" ? null : "Please enter discount"
    });
  };

  calculateTotal = async () => {
    const { price } = this.state;
    const productQuantity = this.state.productList.map(item => item.quantity);
    console.log("price", price);
    console.log("productQuantity", productQuantity);
    const priceWithoutDiscount = price
      .map((price, index) => price * productQuantity[index])
      .reduce((price, quantity) => price + quantity);
    this.setState({ productPrice: priceWithoutDiscount });

    const {
      productList,
      discount,
      productPrice,
      selectedCustomer,
      allProducts
    } = this.state;
    if (allProducts && productPrice && discount !== "") {
      const totalWithDiscount = productPrice - productPrice * (discount / 100);
      const total = (Math.round(totalWithDiscount * 100) / 100).toString();
      await this.setValue("total", total);
    }
  };

  handleProductChange = async (e, i) => {
    console.log("i up", i);
    const { name, value } = e.target;
    let productList = [...this.state.productList];
    productList[i] = { ...productList[i], [name]: value };
    this.setState({ productList });
    await this.setValue(name, value);
    this.singleProductPrice();
  };

  // validateProductNameInput = () => {
  //   const { productList } = this.state;
  //   for (let i = 0; i < productList.length; i++) {
  //     this.setState({
  //       productNameError:
  //         productList[i].aProduct !== "" ? null : "Please select Product"
  //     });
  //   }
  // };

  // editedTotal() {
  //   const { editModal } = this.state;
  //   if (editModal) {
  //     this.calculateTotal();
  //   }
  // }

  formChange = async event => {
    const { name, value } = event.target;
    await this.setValue(name, value);
    if (name == "aProduct") {
    }
    const allProducts = this.state.productList.every(
      prod => prod.aProduct !== ""
    );
    await this.setValue("allProducts", allProducts);
    console.log("st", this.state);
    const { selectedCustomer, discount } = this.state;
    if (selectedCustomer && allProducts && discount !== "") {
      this.calculateTotal();
    }
    const { total, modal, editModal } = this.state;
    if (selectedCustomer && allProducts && discount && total && modal) {
      this.onSubmit();
    }
    if (selectedCustomer && allProducts && discount && total && editModal) {
      this.submitEditedInvoice();
    }
  };

  onSubmit = async event => {
    setTimeout(() => {
      this.postInvoice();
    }, 3000);
  };

  submitEditedInvoice = () => {
    setTimeout(() => {
      this.editInvoice();
    }, 3000);
  };

  deleteInvoice = id => {
    deleteInvoice(id)
      .then(res => {
        this.fetchInvoices();
      })
      .catch(err => this.setState({ deleteError: err.error }));
  };

  editInvoice() {
    const {
      discount,
      total,
      selectedCustomer,
      invoiceId,
      allProducts,
      editModal
    } = this.state;
    const editInvoiceDetails = {
      customer_id: selectedCustomer,
      discount,
      total
    };
    if (
      selectedCustomer &&
      allProducts &&
      discount !== "" &&
      total !== "" &&
      editModal
    ) {
      editInvoice(editInvoiceDetails, invoiceId)
        .then(res => {
          this.postAProduct(res.id);
          this.fetchInvoices();
          this.blankFormField();
          this.setState({ editModal: false });
        })
        .catch(err => this.setState({ editInvoiceError: err.error }));
    }
  }

  postInvoice = async () => {
    const {
      discount,
      total,
      selectedCustomer,
      modal,
      allProducts
    } = this.state;
    const invoiceDetail = {
      customer_id: selectedCustomer,
      discount,
      total
    };
    if (
      selectedCustomer &&
      allProducts &&
      discount !== "" &&
      total !== "" &&
      modal
    ) {
      postInvoice(invoiceDetail)
        .then(res => {
          this.postAProduct(res.id);
          this.fetchInvoices();
          this.setState({
            modal: false,
            editModal: true,
            selectedCustomer: selectedCustomer,
            invoiceId: res.id
          });
        })
        .catch(err => console.log(err.error));
    }
  };

  blankFormField() {
    this.setState({
      selectedCustomer: "",
      productList: [
        {
          aProduct: "",
          quantity: "1"
        }
      ],
      discount: "",
      total: "",
      price: ""
    });
  }

  postAProduct = id => {
    const {
      productList,
      selectedCustomer,
      allProducts,
      discount,
      total,
      modal
    } = this.state;
    for (let i = 0; i < productList.length; i++) {
      const prodDetails = {
        invoice_id: id,
        product_id: productList[i].aProduct,
        quantity: productList[i].quantity
      };
      if (
        selectedCustomer &&
        allProducts &&
        discount !== "" &&
        total !== "" &&
        modal
      ) {
        postAProduct(prodDetails)
          .then(res => {
            this.setState({ aProductPost: res });
          })
          .catch(err => {
            this.setState({ aProductPostError: err.error });
          });
      }
    }
  };

  addProduct = () => {
    this.setState(prevState => ({
      productList: [...prevState.productList, { aProduct: "", quantity: "1" }]
    }));
  };

  getCustomerName = id => {
    const { customers } = this.state;
    const name = customers.filter(cus => cus.id === id).map(cus => cus.name);
    return name;
  };

  // removeProduct = (e, i) => {
  //   console.log("e", e);
  //   console.log("i", i);
  //   let productList = [...this.state.productList];
  //   productList.splice(i, 1);
  //   this.setState({ productList });

  //   e.preventDefault();
  // };

  render() {
    const {
      isLoading,
      invoice,
      error,
      isCustomersLoading,
      customers,
      customerInputError,
      productNameError,
      discountInputError,
      products,
      isProductsLoading,
      total,
      selectedCustomer,
      productList,
      discount,
      modal,
      productPrice,
      price,
      editModal,
      cusIdOfEditableInvoice
    } = this.state;
    return (
      <div>
        <Container>
          <div className="clearfix invoice__head">
            <h2 className="float-left">Invoice</h2>
            <Button
              className="float-right"
              color="primary"
              onClick={this.createInvoiceForm}
            >
              Create Invoice
            </Button>
          </div>

          {/* modal for create invoice */}
          <Modal
            isOpen={this.state.modal || this.state.editModal}
            toggle={this.toggleInvoiceForm}
            className={this.props.className}
          >
            {modal ? (
              <ModalHeader toggle={this.toggleInvoiceForm}>
                Create New Invoice
              </ModalHeader>
            ) : (
              <ModalHeader toggle={this.toggleInvoiceEditForm}>
                Edit Invoice
              </ModalHeader>
            )}

            <ModalBody>
              <Form onChange={() => this.formChange(event)}>
                <Row form>
                  <Col md={12}>
                    <FormGroup>
                      <Label for="selectCustomer">Select Customer</Label>
                      <CustomInput
                        type="select"
                        id="selectCustomer"
                        name="selectedCustomer"
                        value={selectedCustomer}
                        onChange={this.handleCustomer}
                        onBlur={this.validateCustomerInput}
                      >
                        <option value="">Select Customer</option>
                        {!isCustomersLoading ? (
                          customers.map(customer => {
                            return (
                              <option key={customer.id} value={customer.id}>
                                {customer.name}
                              </option>
                            );
                          })
                        ) : (
                          <option>Loading...</option>
                        )}
                      </CustomInput>
                      <p className="text-danger">{customerInputError}</p>
                    </FormGroup>
                  </Col>
                </Row>
                {productList.length > 0
                  ? productList.map((el, i) => {
                      return (
                        <Row form key={i}>
                          <Col md={5}>
                            <FormGroup>
                              <Label for="selectProduct">Select Product</Label>
                              <CustomInput
                                type="select"
                                id="selectProduct"
                                name="aProduct"
                                value={el.aProduct}
                                onChange={() => {}}
                                onInput={e => this.handleProductChange(e, i)}
                                onBlur={this.validateProductNameInput}
                              >
                                <option value="">Select Product</option>
                                {!isProductsLoading ? (
                                  products.map(product => {
                                    return (
                                      <option
                                        key={product.id}
                                        value={product.id}
                                        disabled={productList.find(
                                          p => +p.aProduct === product.id
                                        )}
                                      >
                                        {product.name}
                                      </option>
                                    );
                                  })
                                ) : (
                                  <option>Loading...</option>
                                )}
                              </CustomInput>
                              <p className="text-danger">{productNameError}</p>
                            </FormGroup>
                          </Col>
                          <Col md={2}>
                            <FormGroup>
                              <Label for="price">Price</Label>
                              <p className="mt-2">
                                {price.length > 0 ? price[i] : ""}
                              </p>
                            </FormGroup>
                          </Col>
                          <Col md={2}>
                            <FormGroup>
                              <Label for="numOfProduct">Quantity</Label>
                              <Input
                                type="number"
                                id="numOfProduct"
                                name="quantity"
                                value={el.quantity}
                                onChange={() => {}}
                                onInput={e => this.handleProductChange(e, i)}
                              />
                            </FormGroup>
                          </Col>
                          <Col md={2}>
                            <FormGroup>
                              <Button
                                color="primary"
                                className="invoice__product-btn"
                                onClick={this.addProduct}
                              >
                                Add
                              </Button>
                            </FormGroup>
                          </Col>
                          {/* <Col md={3}>
                      <FormGroup>
                        <Button
                          color="primary"
                          className="invoice__product-btn"
                          disabled={i === 0}
                          onClick={e => this.removeProduct(e, i)}
                        >
                          Remove
                        </Button>
                      </FormGroup>
                    </Col> */}
                        </Row>
                      );
                    })
                  : ""}
                <Row form>
                  <Col md={12}>
                    <FormGroup>
                      <Label for="discount">Discount</Label>
                      <Input
                        type="number"
                        name="discount"
                        id="discount"
                        value={discount}
                        onChange={this.handleDiscount}
                        onBlur={this.validateDiscount}
                      />
                      <p className="text-danger">{discountInputError}</p>
                    </FormGroup>
                  </Col>
                </Row>
              </Form>
              <h3>Total: {total}</h3>
            </ModalBody>
          </Modal>

          <Table bordered>
            <thead>
              <tr>
                <th>Id</th>
                <th>Customer Id</th>
                <th>Discount</th>
                <th>Total</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {!isLoading ? (
                invoice.map(invoice => {
                  return (
                    <tr key={invoice.id}>
                      <th scope="row">{invoice.id}</th>
                      <td>{this.getCustomerName(invoice.customer_id)}</td>
                      <td>{invoice.discount}</td>
                      <td>{invoice.total}</td>
                      <td>{moment(invoice.createdAt).format("llll")}</td>
                      <td>{moment(invoice.updatedAt).format("llll")}</td>
                      <td>
                        <Button
                          color="primary"
                          className="mr-3"
                          onClick={() =>
                            this.openInvoiceEditForm(
                              invoice.customer_id,
                              invoice.id,
                              invoice.discount,
                              invoice.total
                            )
                          }
                        >
                          Edit
                        </Button>
                        <Button
                          color="danger"
                          onClick={() => this.deleteInvoice(invoice.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <th>Loading...</th>
                </tr>
              )}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default Invoice;
