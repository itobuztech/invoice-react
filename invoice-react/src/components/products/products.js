import React, { Component } from "react";

import {
  Table,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import "./products.css";
import {
  getProducts,
  addProduct,
  editProduct,
  deleteProduct
} from "../../services/api";

class Products extends Component {
  state = {
    products: [],
    productsLoading: true,
    productsError: null,
    modal: false,
    editModal: false,
    name: "",
    nameError: "",
    duplicateNameError: "",
    price: "",
    priceError: "",
    isFormValid: false,
    productAddedResponse: null,
    editedProductAddedError: null,
    prodId: null,
    deleteError: null
  };

  getProducts() {
    getProducts()
      .then(data =>
        this.setState({
          products: data,
          productsLoading: false
        })
      )
      .catch(err =>
        this.setState({ productsError: err, productsLoading: true })
      );
  }

  setValue(name, value) {
    return new Promise((res, rej) => {
      this.setState(
        () => {
          return { [name]: value };
        },
        () => res(this.state)
      );
    });
  }

  handleNameChange = async e => {
    const { name, value } = e.target;
    await this.setValue(name, value);
    this.validateName();
  };

  validateName = () => {
    const { name, products } = this.state;
    const allNames = products.map(e => e.name && e.name.toLowerCase());
    const aName = name.toLowerCase();
    let isDuplicateName = allNames.some(name => name === aName);
    this.setState({
      nameError:
        name.length > 3 ? null : "Name must be longer than three characters",
      duplicateNameError: !isDuplicateName
        ? null
        : "Duplicate Name is not allowed"
    });
  };

  handlePriceChange = e => {
    this.setState({ price: e.target.value }, () => {
      this.validatePrice();
    });
  };

  validatePrice = () => {
    const { price } = this.state;
    this.setState({
      priceError: price.length > 0 ? null : "Price cant be empty"
    });
  };

  blankFormField() {
    this.setState({
      name: "",
      price: ""
    });
  }

  clearValidation() {
    this.setState({
      nameError: "",
      duplicateNameError: "",
      priceError: ""
    });
  }

  addProduct = () => {
    const { name, price } = this.state;
    if (!name || !price) return;
    addProduct(name, price)
      .then(res => {
        this.setState({
          productAddedResponse: res.statusText
        });
        this.getProducts();
        this.clearValidation();
        this.toggleAddProduct();
      })
      .catch(err => this.setState({ productAddedResponse: err.error }));
  };

  toggleAddProduct = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
    this.blankFormField();
    this.clearValidation();
  };

  componentDidMount() {
    this.getProducts();
  }

  toggleEditProduct = () => {
    this.setState(prevState => ({
      editModal: !prevState.editModal
    }));
  };

  editProductDetail = (id, name, price) => {
    this.setState(prevState => ({
      editModal: !prevState.editModal,
      prodId: id,
      name,
      price
    }));
    this.clearValidation();
  };

  editProduct() {
    const { name, price, prodId } = this.state;
    const prodDetail = {
      name,
      price
    };
    editProduct(prodDetail, prodId)
      .then(res => {
        this.getProducts();
        this.toggleEditProduct();
      })
      .catch(err => this.setState({ editedProductAddedError: err.error }));
  }

  submitEditProduct = () => {
    this.editProduct();
  };

  deleteProduct = id => {
    deleteProduct(id)
      .then(res => {
        this.getProducts();
      })
      .catch(err => this.setState({ deleteError: err.error }));
  };

  render() {
    const {
      products,
      productsLoading,
      productsError,
      name,
      nameError,
      duplicateNameError,
      price,
      priceError,
      modal,
      editModal,
      isFormValid,
      editName,
      editPrice,
      isEditFormValid
    } = this.state;
    return (
      <div>
        <Container>
          <div className="clearfix invoice__head">
            <h2 className="float-left">Products</h2>
            <Button
              className="float-right"
              color="primary"
              onClick={this.toggleAddProduct}
            >
              Add Product
            </Button>
          </div>

          {/* modal for add product */}
          <Modal
            isOpen={modal || editModal}
            toggle={this.toggleAddProduct}
            className={this.props.className}
          >
            {modal ? (
              <ModalHeader toggle={this.toggleAddProduct}>
                Add Product
              </ModalHeader>
            ) : (
              <ModalHeader toggle={this.toggleEditProduct}>
                Edit Product
              </ModalHeader>
            )}

            <ModalBody>
              <Form>
                <Row form>
                  <Col md={6}>
                    {modal ? (
                      <FormGroup>
                        <Label for="nameOfProduct">Name</Label>
                        <Input
                          type="text"
                          name="name"
                          id="nameOfProduct"
                          value={name}
                          onChange={this.handleNameChange}
                          onBlur={this.validateName}
                        />
                        <p className="text-danger">{nameError}</p>
                        <p className="text-danger">{duplicateNameError}</p>
                      </FormGroup>
                    ) : (
                      <FormGroup>
                        <Label for="nameOfProduct">Name</Label>
                        <Input
                          type="text"
                          name="name"
                          id="nameOfProduct"
                          value={name}
                          readOnly
                        />
                      </FormGroup>
                    )}
                  </Col>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="priceOfProduct">Price</Label>
                      <Input
                        type="number"
                        name="price"
                        id="priceOfProduct"
                        value={price}
                        onChange={this.handlePriceChange}
                        onBlur={this.validatePrice}
                      />
                      <p className="text-danger">{priceError}</p>
                    </FormGroup>
                  </Col>
                </Row>
              </Form>
            </ModalBody>

            <ModalFooter>
              {modal ? (
                <div>
                  <Button
                    color="secondary"
                    className="mr-4"
                    onClick={this.toggleAddProduct}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    disabled={
                      name &&
                      !nameError &&
                      price &&
                      !priceError &&
                      !duplicateNameError
                        ? isFormValid
                        : !isFormValid
                    }
                    onClick={this.addProduct}
                  >
                    Submit
                  </Button>
                </div>
              ) : (
                <div>
                  <Button
                    color="secondary"
                    className="mr-4"
                    onClick={this.toggleEditProduct}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    disabled={price && !priceError ? isFormValid : !isFormValid}
                    onClick={this.submitEditProduct}
                  >
                    Submit
                  </Button>
                </div>
              )}
            </ModalFooter>
          </Modal>
          {productsError ? (
            <div>
              <p>{this.productsError}</p>
            </div>
          ) : null}
          <Table bordered>
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {!productsLoading ? (
                products.map(product => {
                  return (
                    <tr key={product.id}>
                      <td>{product.id}</td>
                      <td>{product.name}</td>
                      <td>{product.price}</td>
                      <td>{product.createdAt}</td>
                      <td>{product.updatedAt}</td>
                      <td>
                        <Button
                          color="primary"
                          className="mr-3"
                          onClick={() =>
                            this.editProductDetail(
                              product.id,
                              product.name,
                              product.price
                            )
                          }
                        >
                          Edit
                        </Button>
                        <Button
                          color="danger"
                          onClick={() => this.deleteProduct(product.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <th>Loading ...</th>
                </tr>
              )}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default Products;
