import React, { Component } from "react";

import {
  Table,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import "./customers.css";
import {
  getCustomers,
  addCustomer,
  updateCustomer,
  deleteCustomer
} from "./../../services/api";

class Customers extends Component {
  state = {
    customers: [],
    customersLoading: true,
    customersError: null,
    modal: false,
    name: "",
    nameError: "",
    duplicateNameError: "",
    address: "",
    addressError: "",
    phone: "",
    phoneError: "",
    isFormValid: false,
    customerAddedResponse: null,
    updateModal: false,
    editFromError: null,
    cusId: null,
    editForm: false,
    deleteError: null
  };

  getCustomers() {
    getCustomers()
      .then(data =>
        this.setState({
          customers: data,
          customersLoading: false
        })
      )
      .catch(err =>
        this.setState({ customersError: err, customersLoading: true })
      );
  }

  handleNameChange = e => {
    this.setState({ name: e.target.value }, () => {
      this.validateName();
    });
  };

  validateName = () => {
    const { name, customers } = this.state;
    const allLoweredNames = customers.map(
      customer => customer.name && customer.name.toLowerCase()
    );
    const lowerName = name.toLowerCase();
    let isDuplicateName = allLoweredNames.some(name => name === lowerName);
    this.setState({
      nameError:
        name.length > 3 ? null : "Name must be longer than three characters",
      duplicateNameError: !isDuplicateName
        ? null
        : "Duplicate Name is not allowed"
    });
  };

  handleAddressChange = e => {
    this.setState({ address: e.target.value }, () => {
      this.validateAddress();
    });
  };

  handlePhoneChange = e => {
    this.setState({ phone: e.target.value }, () => {
      this.validatePhone();
    });
  };

  validateAddress = () => {
    const { address } = this.state;
    this.setState({
      addressError:
        address.length > 5
          ? null
          : "Address must be longer than five characters"
    });
  };

  validatePhone = () => {
    const { phone } = this.state;
    const phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    this.setState({
      phoneError: phone.match(phoneno)
        ? null
        : "Phone number must be number only"
    });
  };

  addCustomer = () => {
    const { name, address, phone } = this.state;
    if (!name || !address || !phone) return;
    const cusDetails = {
      name,
      address,
      phone
    };
    addCustomer(cusDetails)
      .then(res => {
        this.setState({
          customerAddedResponse: res.statusText
        });
        this.getCustomers();
        this.toggleAddCustomer();
      })
      .catch(err => this.setState({ customerAddedResponse: err.error }));
  };

  blankCustomerForm() {
    this.setState({
      name: "",
      address: "",
      phone: ""
    });
  }

  clearValidation() {
    this.setState({
      nameError: "",
      duplicateNameError: "",
      addressError: "",
      phoneError: ""
    });
  }

  toggleAddCustomer = () => {
    this.setState(prevState => ({
      modal: !prevState.modal,
      editForm: true
    }));
    this.blankCustomerForm();
    this.clearValidation();
  };

  toggleEditCustomer = () => {
    this.setState(prevState => ({
      updateModal: !prevState.updateModal,
      editForm: false
    }));
  };

  editCustomerDetails = (id, name, address, phone) => {
    this.setState(prevState => ({
      updateModal: !prevState.updateModal,
      editForm: false,
      cusId: id,
      name,
      address,
      phone
    }));
  };

  submitEditedCustomer = () => {
    this.editCustomer();
  };

  editCustomer() {
    const { name, address, phone, cusId } = this.state;
    const cusDetails = {
      name: name,
      address: address,
      phone: phone
    };
    updateCustomer(cusDetails, cusId)
      .then(res => {
        this.getCustomers();
        this.toggleEditCustomer();
      })
      .catch(err => this.setState({ editFromError: err.error }));
  }

  deleteCustomer = id => {
    deleteCustomer(id)
      .then(res => {
        this.getCustomers();
      })
      .catch(err => this.setState({ deleteError: err.error }));
  };

  componentDidMount() {
    this.getCustomers();
  }

  render() {
    const {
      customers,
      customersLoading,
      customersError,
      name,
      nameError,
      duplicateNameError,
      address,
      addressError,
      phone,
      phoneError,
      isFormValid,
      editForm
    } = this.state;
    return (
      <div>
        <Container>
          <div className="clearfix invoice__head">
            <h2 className="float-left">Customers</h2>
            <Button
              className="float-right"
              color="primary"
              onClick={this.toggleAddCustomer}
            >
              Add Customer
            </Button>
          </div>
          <Modal
            isOpen={this.state.modal || this.state.updateModal}
            toggle={this.toggleAddCustomer}
            className={this.props.className}
          >
            {editForm ? (
              <ModalHeader toggle={this.toggleAddCustomer}>
                Add Customer
              </ModalHeader>
            ) : (
              <ModalHeader toggle={this.toggleEditCustomer}>
                Edit Customer
              </ModalHeader>
            )}

            <ModalBody>
              <Form>
                <Row form>
                  <Col md={12}>
                    {editForm ? (
                      <FormGroup>
                        <Label for="nameOfCustomer">Name</Label>
                        <Input
                          type="text"
                          name="name"
                          id="nameOfCustomer"
                          value={name}
                          onChange={this.handleNameChange}
                          onBlur={this.validateName}
                        />
                        <p className="text-danger">{nameError}</p>
                        <p className="text-danger">{duplicateNameError}</p>
                      </FormGroup>
                    ) : (
                      <FormGroup>
                        <Label for="nameOfCustomer">Name</Label>
                        <Input
                          type="text"
                          name="name"
                          id="nameOfCustomer"
                          value={name}
                          readOnly
                        />
                      </FormGroup>
                    )}
                  </Col>
                  <Col md={12}>
                    <FormGroup>
                      <Label for="priceOfProduct">Address</Label>
                      <Input
                        type="text"
                        name="address"
                        id="priceOfProduct"
                        value={address}
                        onChange={this.handleAddressChange}
                        onBlur={this.validateAddress}
                      />
                      <p className="text-danger">{addressError}</p>
                    </FormGroup>
                  </Col>
                  <Col md={12}>
                    <FormGroup>
                      <Label for="phone">Phone</Label>
                      <Input
                        type="text"
                        name="phone"
                        id="phone"
                        maxLength={10}
                        value={phone}
                        onChange={this.handlePhoneChange}
                        onBlur={this.validatePhone}
                      />
                      <p className="text-danger">{phoneError}</p>
                    </FormGroup>
                  </Col>
                </Row>
              </Form>
            </ModalBody>
            <ModalFooter>
              {editForm ? (
                <div>
                  <Button
                    color="secondary"
                    className="mr-3"
                    onClick={this.toggleAddCustomer}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    disabled={
                      name &&
                      !nameError &&
                      address &&
                      !addressError &&
                      phone &&
                      !phoneError &&
                      !duplicateNameError
                        ? isFormValid
                        : !isFormValid
                    }
                    onClick={this.addCustomer}
                  >
                    Submit
                  </Button>
                </div>
              ) : (
                <div>
                  <Button
                    color="secondary"
                    className="mr-3"
                    onClick={this.toggleEditCustomer}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    disabled={
                      name &&
                      !nameError &&
                      address &&
                      !addressError &&
                      phone &&
                      !phoneError &&
                      !duplicateNameError
                        ? isFormValid
                        : !isFormValid
                    }
                    onClick={this.submitEditedCustomer}
                  >
                    Submit
                  </Button>
                </div>
              )}
            </ModalFooter>
          </Modal>

          {customersError ? "<p>{customersError}</p>" : null}
          <Table bordered>
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {!customersLoading ? (
                customers.map(customer => {
                  return (
                    <tr key={customer.id}>
                      <td>{customer.id}</td>
                      <td>{customer.name}</td>
                      <td>{customer.address}</td>
                      <td>{customer.phone}</td>
                      <td>{customer.createdAt}</td>
                      <td>{customer.updatedAt}</td>
                      <td className="customers__edit-delete">
                        <Button
                          color="primary"
                          className="mr-3"
                          onClick={() =>
                            this.editCustomerDetails(
                              customer.id,
                              customer.name,
                              customer.address,
                              customer.phone
                            )
                          }
                        >
                          Edit
                        </Button>
                        <Button
                          color="danger"
                          onClick={() => this.deleteCustomer(customer.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <th>Loading ...</th>
                </tr>
              )}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default Customers;
