import React, { Component } from "react";

import {
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button,
  ModalFooter,
  Modal,
  ModalBody,
  ModalHeader
} from "reactstrap";

import { fetchInvoices, editInvoice } from "../services/api";

class EditInvoice extends Component {
  state = {
    discount: "",
    total: "",
    isEditFormValid: false,
    updatedInvoice: null,
    updatedInvoiceError: null
  };
  toggleInvoiceEditForm = () => {
    this.props.toggle();
    console.log("props", this.props);
  };

  updateInvoiceData = () => {};

  handleInputChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
    console.log("name", name);
    console.log("value", value);
  };

  submitInvoice = () => {
    // const { discount, total } = this.state;
    // const invoiceDetails = {
    //   customer_id: this.props.cusId,
    //   discount,
    //   total
    // };
    // editInvoice(invoiceDetails)
    //   .then(res => {
    //     this.setState({ updatedInvoice: res });
    //     // this.toggleInvoiceEditForm();

    //   })
    //   .catch(err => {
    //     this.setState({ updatedInvoiceError: err.error });
    //     // this.toggleInvoiceEditForm();
    //   });
    console.log(Modal.prototype);
    // Modal.prototype.close();
    // if (Modal.prototype.isOpen) {
    //   Modal.prototype.toggle();
    // }
  };

  render() {
    const { discount, total } = this.state;
    return (
      <Modal
        isOpen={this.props.open}
        toggle={this.toggleInvoiceEditForm}
        onExit={this.updateInvoiceData}
      >
        <ModalHeader toggle={this.toggleInvoiceEditForm}>
          Edit Invoice
        </ModalHeader>
        <ModalBody>
          <Form>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="prodDiscount">Discount</Label>
                  <Input
                    type="number"
                    name="discount"
                    id="prodDiscount"
                    value={discount}
                    onChange={this.handleInputChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="prodTotal">Total</Label>
                  <Input
                    type="number"
                    name="total"
                    id="prodTotal"
                    value={total}
                    onChange={this.handleInputChange}
                  />
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggleInvoiceEditForm}>
            Cancel
          </Button>
          <Button color="primary" onClick={this.submitInvoice}>
            Submit
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default EditInvoice;
