import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import Invoice from "./components/invoice/invoice";
import Products from "./components/products/products";
import Customers from "./components/customers/customers";
import Notfound from "./components/notfound/notfound";

const routes = (
  <Switch>
    <Route exact path="/" component={Invoice} />
    <Route path="/products" component={Products} />
    <Route path="/customers" component={Customers} />
    <Route component={Notfound} />
  </Switch>
);

export default routes;
