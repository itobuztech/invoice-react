export const fetchInvoices = async () => {
  const response = await fetch(`/api/invoices`);
  const body = await response.json();
  return body;
};

export const postInvoice = async invoDetail => {
  const response = await fetch(`/api/invoices`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(invoDetail)
  });
  const body = await response.json();
  return body;
};

export const editInvoice = async (invoDetails, id) => {
  const response = await fetch(`/api/invoices/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(invoDetails)
  });
  const body = await response.json();
  return body;
};

export const deleteInvoice = async id => {
  const response = await fetch(`api/invoices/${id}`, {
    method: "DELETE"
  });
  const body = await response.json();
  return body;
};

export const getProducts = async () => {
  const response = await fetch(`/api/products`);
  const body = await response.json();
  return body;
};

export const addProduct = async (name, price) => {
  const response = await fetch(`/api/products`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ name: `${name}`, price: `${price}` })
  });
  const body = await response.json();
  return body;
};

export const postAProduct = async prodDetail => {
  const response = await fetch(`/api/invoices/${prodDetail.invoice_id}/items`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(prodDetail)
  });
  const body = await response.json();
  return body;
};

export async function editAProduct(prodDetails) {
  const response = await fetch(
    `/api/invoices/${prodDetails.invoice_id}/items/${prodDetails.id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(prodDetails)
    }
  );
  const body = await response.json();
  return body;
}

export async function getAProduct(id) {
  const response = await fetch(`/api/invoices/${id}/items`);
  const body = await response.json();
  return body;
}

export const editProduct = async (prodDetail, id) => {
  const response = await fetch(`/api/products/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(prodDetail)
  });
  const body = await response.json();
  return body;
};

export const deleteProduct = async id => {
  const response = await fetch(`api/products/${id}`, {
    method: "DELETE"
  });
  const body = await response.json();
  return body;
};

export const getCustomers = async () => {
  const response = await fetch(`api/customers`);
  const body = await response.json();
  return body;
};

export const addCustomer = async cusDetail => {
  const response = await fetch(`api/customers`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(cusDetail)
  });
  const body = await response.json();
  return body;
};

export const updateCustomer = async (cusDetail, id) => {
  const response = await fetch(`/api/customers/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(cusDetail)
  });
  const body = await response.json();
  return body;
};

export const deleteCustomer = async id => {
  const response = await fetch(`api/customers/${id}`, {
    method: "DELETE"
  });
  const body = await response.json();
  return body;
};
